;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023 Petr Hodina <phodina@protonmail.com>

(define-module (nongnu services grafana)
  #:use-module (gnu packages)
  #:use-module (nongnu packages grafana)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:export (grafana-configuration
            grafana-configuration?
            grafana-configuration-record?
            grafana-service-type))

(define-record-type* <grafana-configuration>
  grafana-configuration make-grafana-configuration
  grafana-configuration?
  ;; TODO: Port, HTTPS, plugins?
  (grafana grafana-configuration-package
                   (default (list grafana-bin))))         ; package

;; Add config file
(define (grafana-shepherd-service config)
  (list (shepherd-service
         (documentation "Run Grafana server.")
         (provision '(grafana))
         (requirement '(user-processes))
		 (start #~(make-forkexec-constructor
                      (list (string-append grafana
                                           "/bin/grafana-server")
					  "-homepath" grafana)))
            (stop #~(make-kill-destructor)))))

(define grafana-service-type
  (service-type
   (name 'grafana)
   (extensions
    (list (service-extension shepherd-root-service-type
                             grafana-shepherd-service)))
   (default-value (grafana-configuration))
   (description "Run Grafana server.")))
