(define-module (nongnu packages cloud)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (nonguix build-system binary)
  #:use-module (guix download))

(define-public yandex-cloud-cli
  (let ((arch-match (lambda (arch)
                      (match arch
                        ((or "x86_64-linux") "amd64")
                        ((or "i686-linux") "386")
                        ((or "aarch64-linux") "arm64")))))
    (package
      (name "yandex-cloud-cli")
      (version "0.113.0")
      (source
       (origin
         (method url-fetch)
         (uri (string-append
               "https://storage.yandexcloud.net/yandexcloud-yc/release/"
               version "/linux/"
               (arch-match
                (or (%current-target-system)
                    (%current-system)))
               "/yc"))
         (sha256
          (base32
           "0v8ypci9xqvapj4ls4w7ay663d1s3icy58rr174iasdyqh97mjj9"))))
      (build-system binary-build-system)
      (arguments
       `(#:install-plan
         `(("yc" "/bin/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'chmod
             (lambda _
               (chmod "yc" #o755))))))
       (inputs `())
       (supported-systems '("x86_64-linux"
                            "i686-linux"
                            "aarch64-linux"))
       (synopsis "The Yandex Cloud command-line interface (CLI) provides downloadable software for managing your cloud resources from the command line")
       (description "")
       (home-page "https://cloud.yandex.com/en-ru/docs/cli/quickstart")
       (license apsl2))))
