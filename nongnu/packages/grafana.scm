;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023 Petr Hodina <phodina@protonmail.com>

(define-module (nongnu packages grafana)
  #:use-module (guix packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages glib)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public grafana-bin
  (package
    (name "grafana")
    (version "9.3.2")
    (source (cond
              ((target-x86-64?)
               (origin
                 (method url-fetch)
                 (uri (string-append
                       "https://dl.grafana.com/oss/release/grafana-" version
                       ".linux-amd64.tar.gz"))
                 (sha256 (base32
                          "177d396sg6pwa7vwsdwqy6fg17kq47n619s4745z62s6inq3i0vr"))))
              ((target-aarch64?)
               (origin
                 (method url-fetch)
                 (uri (string-append
                       "https://dl.grafana.com/oss/release/grafana-" version
                       ".linux-arm64.tar.gz"))
                 (sha256 (base32
                          "1ps8aa279fh8hcngda794f69w6hm78pw9pgvwyxlnx229581zrkv"))))
              ((target-armhf?)
               (origin
                 (method url-fetch)
                 (uri (string-append
                       "https://dl.grafana.com/oss/release/grafana-" version
                       ".linux-armv7.tar.gz"))
                 (sha256 (base32
                          "0avndch41m5k7gpxzdk8k1gzz3nyscmw3va4mk5813vbfmynv2bn"))))))
    (build-system binary-build-system)
    (arguments
     '(#:strip-binaries? #f ;TODO: For some reason it fails validate-runpath
       ;; phase if enabled
       #:install-plan `(("bin/grafana-cli" "bin/grafana-cli")
                        ("bin/grafana-server" "bin/grafana-server")
                        ("conf/defaults.ini" "etc/grafana/grafana.ini")
                        ("conf" "share/grafana/")
                        ("public" "share/grafana/")
                        ("scripts" "share/grafana/")
                        ("plugins-bundled" "share/grafana/"))
       #:patchelf-plan (list (list "bin/grafana-cli"
                                   '("glibc" "gcc:lib"))
                             (list "bin/grafana-server"
                                   '("glibc" "gcc:lib")))))
    (supported-systems '("x86_64-linux" "aarch64-linux" "armhf-linux"))
    (native-inputs (list patchelf))
    (inputs `(("gcc:lib" ,gcc "lib")
              ("glibc" ,glibc)))
    (synopsis "Platform for monitoring and observability")
    (description
     "Grafana allows you to query, visualize, alert on
and understand your metrics no matter where they are stored.  Create, explore,
and share dashboards with your team and foster a data-driven culture:
@enumerate
@ite Visualizations: Fast and flexible client side graphs with a multitude
of options.  Panel plugins offer many different ways to visualize metrics
and logs
@item Dynamic Dashboards: Create dynamic & reusable dashboards with template
variables that appear as dropdowns at the top of the dashboard.
@item Explore Metrics: Explore your data through ad-hoc queries and dynamic
drilldown.  Split view and compare different time ranges, queries and data
sources side by side.
@item Explore Logs: Experience the magic of switching from metrics to logs with
preserved label filters. Quickly search through all your logs or streaming them
live.
@item Alerting: Visually define alert rules for your most important metrics.
Grafana will continuously evaluate and send notifications to systems like Slack,
PagerDuty, VictorOps, OpsGenie.
@item Mixed Data Sources: Mix different data sources in the same graph! You can
specify a data source on a per-query basis. This works for even custom
datasources.
@end enumerate")
    (home-page "https://grafana.com/")
    (license license:agpl3)))
