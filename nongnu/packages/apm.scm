(define-module (nongnu packages apm)
  #:use-module (ice-9 match)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (nonguix build-system binary)
  #:use-module (guix licenses))

(define-public newrelic-cli
  (let ((arch-match (lambda (arch)
                      (match arch
                        ((or "x86_64-linux") "x86_64")
                        ((or "aarch64-linux") "arm64")))))
    (package
      (name "newrelic-cli")
      (version "0.76.3")
      (source
       (origin
         (method url-fetch)
         (uri (string-append
               "https://download.newrelic.com/install/newrelic-cli/v"
               version "/newrelic-cli_" version "_Linux_"
               (arch-match
                (or (%current-target-system)
                    (%current-system)))
               ".tar.gz"))
         (sha256
          (base32
           "1v9sz89myszlqa6012n0x6vmfj4vvpzmwdp62llfxv204v55wnb4"))))
      (build-system binary-build-system)
      (arguments
       `(#:install-plan
         `(("newrelic" "/bin/"))))
      (inputs `())
      (supported-systems '("x86_64-linux"
                           "aarch64-linux"))
      (synopsis "Access the New Relic platform from the comfort of your terminal")
      (description "You can use the New Relic CLI to manage entity tags, define workloads, record deployment markers. So you can use the CLI to automate common tasks in your DevOps workflow.")
      (home-page "docs.newrelic.com/docs/new-relic-solutions/tutorials/new-relic-cli")
      (license apsl2))))
