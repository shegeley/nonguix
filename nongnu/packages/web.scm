(define-module (nongnu packages web)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (nonguix build-system binary)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc))

(define-public mitmproxy
  (package
    (name "mitmproxy")
    (version "10.1.5")
    (source
     (origin
       (method url-fetch/tarbomb)
       (uri
        (string-append "https://downloads.mitmproxy.org/" version "/mitmproxy-" version "-linux.tar.gz"))
       (sha256
        (base32
         "12kdgbns2sf3kwdnqs91l7mqyf1yyv765rnmbivgmxa0qdkvxry4"))))
    (build-system binary-build-system)
    (arguments
     `(#:strip-binaries? #f
       #:patchelf-plan
       '(("mitmproxy" ("gcc:lib" "zlib"))
         ("mitmweb" ("gcc:lib" "zlib"))
         ("mitmdump" ("gcc:lib" "zlib")))
       #:install-plan
       '(("mitmproxy" "/bin/")
         ("mitmweb" "/bin/")
         ("mitmdump" "/bin/"))))
    (inputs
     `(("gcc:lib" ,gcc "lib")
       ("zlib" ,zlib)))
    (home-page "https://mitmproxy.org/")
    (supported-systems '("x86_64-linux"))
    (synopsis "Free and open source interactive HTTPS proxy")
    (description
     "mitmproxy is an interactive, SSL/TLS-capable intercepting proxy with a console interface for HTTP/1, HTTP/2, and WebSockets; mitmdump is the command-line version of mitmproxy; mitmweb is a web-based interface for mitmproxy;")
    (license license:expat)))
